# pruebaPeakU

En esta ocasión se realizó la prueba para desarrollo backend* junior JAVA.

Se utilizó un patrón de diseño MVC con POO usando el framewors de hibernet para el ORM de la base de datos, con el fin de
facilitar las operaciones con la BD y aumentar la seguridad del aplicativo.

El problema a abarcar fue el siguiente:

Una agencia de empleo necesita la construcción de una aplicación web que le permita administrar
la información básica de los aspirantes a los cargos que se encuentra ofertados. Los aspirantes
deben registrarse ingresando por la página ingresando los datos de nombre, el número de cedula,
edad, genero y profesión. Se necesita que la aplicación, muestre cuantos aspirantes tienen nombre
que empieza por una determinante letra, una oferta de empleo es inhabilitada cuando ya tiene 5
aspirantes registrados y no debemos permitir mas registros para esta oferta. Los aspirantes solo
solo están disponibles por 5 días hábiles en el sistema, después de eso se inhabilita quedando solo
los aspirantes registrados a ella.
Adicionalmente se desea saber cual es el aspirante con mayor edad y también se necesita conocer
la lista de los aspirantes por profesión.
La agencia cuenta con un listado de profesiones definidas.

Entre las dificultades presentadas nos encontramos con la poca experiencia en el manejo de JSP integrado con hibernate, ya que el front no se
me ha dado muy bien, a diferencia del back. Por tal motivo se gastó más del tiempo necesario en la vista. 

**----**

El proyecto fue realizado en Eclipse como Dynamic Web Proyect. Al descargar el branch solo hace falta abrir el proyecto desde el IDE mencionado.
Con respecto a la base de datos, tambien se encuentra el archivo sql de la creación de la BD. 


<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="modelo.*"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<title>AGENCIA DE EMPLEOS</title>
</head>
<body style="margin-top: 30px" style="text-align: center;">
	
	<% session.setAttribute("nombre", ""); %>
	<jsp:include page="componentes/header.jsp" />
	<% List<Oferta> ofertas = null;
		Aspirante aspirante = new Aspirante();
		Agencia agencia = new Agencia();
		
		ofertas = (List<Oferta>) session.getAttribute("Ofertas");
		List<Aspirante> aspirantes = (List<Aspirante>) session.getAttribute("Aspirantes");
		List<Empleabilidad> empleos = (List<Empleabilidad>) session.getAttribute("OfertasAplicadas");
		
		
		%>
		
	<form action="Controller" method="post">
	
	
		<table class="table">
	<thead>
	<tr>
		<th scope="col">Oferta</th>
		<th scope="col">Descripción</th>
		<th scope="col">Fecha</th>
		<th scope="col">Aspirante</th>
	</tr>
	</thead>
	 <tbody>
	 <%for (Oferta oferta : ofertas) { %>
    <tr>
      <td><%=oferta.getNombre()%></td>
      <td><%=oferta.getDescripcion()%></td>
      <td><%=oferta.getFechaInicio()%></td>
      <td></td>
    </tr>
    <%} %>
    </tbody>
	</table>
	</form>
	<jsp:include page="componentes/footer.jsp" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
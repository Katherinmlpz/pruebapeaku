<%@page import="modelo.*"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<title>AGENCIA DE EMPLEOS</title>
</head>
<body style="margin-top: 30px" style="text-align: center;">

	<%
		List<Oferta> ofertas = null;
		Aspirante aspirante = new Aspirante();
		Agencia agencia = new Agencia();
		String rol = request.getParameter("rol");
		if (rol.equals("1")) {
			aspirante = (Aspirante) session.getAttribute("datos");
			session.setAttribute("nombre", aspirante.getNombre());
			ofertas = (List<Oferta>) session.getAttribute("listaOfertasProfesion");
		} else {
			agencia = (Agencia) session.getAttribute("datos");
			session.setAttribute("nombre", agencia.getNombre());
			session.setAttribute("idAgencia", agencia.getIdAgencia());
		}

		
		List<Profesion> profesiones = (List<Profesion>) session.getAttribute("profesiones");
	%>

	<jsp:include page="componentes/header.jsp" />
	<div style="text-align: center;">

		<%
			if (rol.equals("2")) {
		%>
		<h1>OFERTAS DE EMPLEO PUBLICADAS</h1>
		<form action="Controller">
		<input type="hidden" name="idAgencia" value="<%=agencia.getIdAgencia()%>">
		<%
			for (Profesion profesion : profesiones) {
		%>
		<br>
		<div>
			<button class="btn btn-danger btn-lg" data-toggle="modal"
				name="Opcion" value="<%=profesion.getIdProfesion()%>"
				type="submit"><%=profesion.getpNombre()%></button>
		</div>

		<%
			}
		%>
		<br>
		<div>
			<button class="btn btn-success btn-lg" data-toggle="modal" name="Opcion"
				type="submit" value="Gestionar">GESTIONAR ASPIRANTES</button>
		</div>
		
		</form>
		<!-- Contenerdo y ventana emergente para agregar usuarios aspirantes -->
		
		<%
			} else if(!ofertas.isEmpty()){
		%>

		<h1>OFERTAS DISPONIBLES</h1>
	<form action="Controller" method="post">
	
	
		<table class="table">
	<thead>
	<tr>
		<th scope="col">Nombre</th>
		<th scope="col">Descripción</th>
		<th scope="col">Fecha Inicio</th>
		<th scope="col">Fecha Fin</th>
		<th scope="col">Acción</th>
	</tr>
	</thead>
	 <tbody>
	 <%for (Oferta oferta : ofertas) { %>
    <tr>
      <td><%=oferta.getNombre()%></td>
      <td><%=oferta.getDescripcion()%></td>
      <td><%=oferta.getFechaInicio()%></td>
      <td><%=oferta.getFechaFin()%></td>
      <input type="hidden" name="idAspirante" value="<%=aspirante.getIdAspirante()%>">
      <input type="hidden" name="fecha" value="<%=oferta.getFechaInicio()%>">
      <input type="hidden" name="idProfesion" value="<%=oferta.getIdProfesion()%>">
      <input type="hidden" name="idOferta" value="<%=oferta.getIdOferta()%>">
      <td><button class="btn btn-success" data-toggle="modal" name="Opcion"
				type="submit" value="Aplicar">Aplicar</button></td>
    </tr>
    <%} %>
    </tbody>
	</table>
	</form>
	<%}else{ %>
	<h1>NO TIENE OFERTAS DISPONIBLES</h1>
	<%} %>


	<jsp:include page="componentes/footer.jsp" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
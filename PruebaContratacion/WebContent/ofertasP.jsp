<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="modelo.*"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<title>AGENCIA DE EMPLEOS</title>
</head>
<body style="margin-top: 30px" style="text-align: center;">
	
	<% session.setAttribute("nombre", ""); %>
	<jsp:include page="componentes/header.jsp" />
	<div style="text-align: center;">
	<% 
	List<Oferta> ofertas = 	(List<Oferta>) session.getAttribute("listaOfertasProfesion");
	if(ofertas.isEmpty()){
	%> <h1> ¡Vaya!, parece que no tiene ofertas publicadas </h1>
	
	<% }else{ %>
	<table class="table">
	<thead>>
	<tr>
		<th scope="col">Nombre</th>
		<th scope="col">Descripción<th>
		<th scope="col">Fecha Inicio</th>
		<th scope="col">Fecha Fin</th>
		<th scope="col">Acción</th>
	</tr>
	</thead>
	 <tbody>
	 <%for (Oferta oferta : ofertas) { %>
    <tr>
      <td><%=oferta.getNombre()%></th>
      <td><%=oferta.getDescripcion()%></td>
      <td><%=oferta.getFechaInicio()%></td>
      <td><%=oferta.getFechaFin()%></td>
      <td><button class="btn btn-danger" data-toggle="modal"
				type="submit" value="Eliminar">Eliminar</button></td>
    </tr>
    <%} %>
    </tbody>
	</table>
	<%} %>
	<br>
	<div>
	<button class="btn btn-success btn-lg" data-toggle="modal"
				data-target="#ofertasEmpresa">PUBLICAR OFERTA</button>
	</div>
	</div>
	
	<!-- Contenerdo y ventana emergente para agregar usuarios aspirantes -->
	<div class="container">
		<div class="modal fade" id="ofertasEmpresa" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document"
				style="z-index: 9999; width: 450px">
				<div class="modal-content" style="width: 419px;">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Agregar Oferta</h4>
					</div>
					<div class="modal-body">
						<form action="Controller" method="post"
							style="text-align: center;">
							 <label>NOMBRE:</label> <input
								type="text" name="nombre" class="form-control" required /> 
								<label>DESCRIPCION:</label>
							<input type="text" maxlength="50" name="descripcion"
								class="form-control" required /> 
							<label>FECHA INICIO:</label>
							<input type="date" name="fecha_inicio"
								class="form-control" required />
							<label>FECHA FIN:</label>
							<input type="date" name="fecha_fin"
								class="form-control" required />
							<input type="hidden" name="idAgencia" value="<%=session.getAttribute("idAgencia")%>">
							<input type="hidden" name="idProfesion" value="<%=session.getAttribute("idProfesion")%>">
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">CERRAR</button>
								<input type="submit" value="Publicar" name="Opcion"
									class="btn btn-primary" />

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	

		
	<jsp:include page="componentes/footer.jsp" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
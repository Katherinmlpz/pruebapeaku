<%@page import="modelo.*"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<title>AGENCIA DE EMPLEOS</title>
</head>
<body style="margin-top: 30px" style="text-align: center;">
	
	<% session.setAttribute("nombre", ""); %>
	<jsp:include page="componentes/header.jsp" />
	<form action="Controller" method="post">

		<div class="container" style="text-align: center; width: 1008px">
			<div class="Icon">
				<img alt="Rol" src="imagenes/aspirante.png">
			</div>
			<br> <label> Rol: <select required name="rol"
				style="background-color: WHITE">
					<option value="1">ASPIRANTE</option>
					<option value="2">EMPRESA</option>
			</select>
			</label> <br> <label>Cedula/Nit:</label> <input type="number"
				name="identificacion" class="form-control" required /> <br> <label>Contraseña:</label>
			<input type="password" name="contrasena" maxlength="8"
				class="form-control" required /> <br>
			<div>
				<button class="btn btn-success btn-lg" value="IniciarSesion" name="Opcion"
					type="submit">Iniciar Sesión</button>
			</div>
		</div>
	</form>
	<br>
	<div class="container" style="text-align: center;">
		<button class="btn btn-danger btn-lg" data-toggle="modal"
			data-target="#registroA">Registrar Aspirante</button>
		<button class="btn btn-danger btn-lg" data-toggle="modal"
			data-target="#registroE">Registrar Empresa</button>
	</div>
	<!-- Contenerdo y ventana emergente para agregar usuarios aspirantes -->
	<div class="container">
		<div class="modal fade" id="registroA" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document"
				style="z-index: 9999; width: 450px">
				<div class="modal-content" style="width: 419px;">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Agregar Usuario</h4>
					</div>
					<div class="modal-body">
						<form action="Controller" method="post"
							style="text-align: center;">
							<div class="Icon">
								<img alt="Rol" src="imagenes/aspirante.png">
							</div>
							<label>CEDULA:</label> <input type="number" name="cedula"
								class="form-control" required /> <label>NOMBRE:</label> <input
								type="text" name="nombre" class="form-control" required /> <label>EDAD:</label>
							<input type="number" maxlength="2" name="edad"
								class="form-control" required /> <label>GENERO:</label> <label>
								<input type="radio" name="genero" value="M"> M
							</label> <label> <input type="radio" name="genero" value="F">
								F
							</label> <br> <label> PROFESION: <select required
								name="profesion" style="background-color: WHITE">
									<option value="1">ABOGADO</option>
									<option value="2">INGENIERIA DE SISTEMAS</option>
									<option value="3">ADMINISTRACION DE EMPRESAS</option>
									<option value="4">PSICOLOGO</option>

							</select>
							</label> <br> <label>Contraseña:</label> <input type="password"
								name="contrasena" maxlength="8" class="form-control" required />
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">CERRAR</button>
								<input type="submit" value="RegistrarAspirante" name="Opcion"
									class="btn btn-primary" />

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Contenerdo y ventana emergente para agregar usuarios empresas -->
	<div class="container">
		<div class="modal fade" id="registroE" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document"
				style="z-index: 9999; width: 450px">
				<div class="modal-content" style="width: 419px;">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Agregar Usuario</h4>
					</div>
					<div class="modal-body">
						<form action="Controller" method="post" style="text-align: center;">
							<div class="Icon">
								<img alt="Rol" src="imagenes/empresa.png">
							</div>
							<label>Nit:</label> <input type="number" name="nit"
								class="form-control" required /> <label>NOMBRE DE LA
								AGENCIA:</label> <input type="text" name="nombre" class="form-control"
								required /> <label>TELEFONO:</label> <input type="number"
								maxlength="11" name="telefono" class="form-control" required /> <br>
							<label> DIRECCION: </label> <input type="text" name="direccion"
								class="form-control" required /> <br> <label>Contraseña:</label>
							<input type="password" name="contrasena" maxlength="8"
								class="form-control" required />
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">CERRAR</button>
								<input type="submit" value="RegistrarAgencia" name="Opcion"
									class="btn btn-primary" />

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="componentes/footer.jsp" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
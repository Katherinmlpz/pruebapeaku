package modelo;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@Entity
@Table(name = "tb_agencia")
public class Agencia {

	public Agencia() {
		super();
	}

	public Agencia(int idAgencia, String nombre, String telefono, String direccion) {
		super();
		this.idAgencia = idAgencia;
		this.nombre = nombre;
		this.telefono = telefono;
		this.direccion = direccion;
	}

	@Id
	@Column(name = "IdAgencia")
	private int idAgencia;
	@Column(name = "Ag_Nombre")
	private String nombre;
	@Column(name = "Ag_Telefono")
	private String telefono;
	@Column(name = "Ag_Direccion")
	private String direccion;

	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Agencia [idAgencia=" + idAgencia + ", nombre=" + nombre + ", telefono=" + telefono + ", direccion="
				+ direccion + "]";
	}

	public boolean registrarAgencia(Agencia agencia) {
		System.out.println("Registrando Agencia");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Agencia.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Session.beginTransaction();
			Session.save(agencia);
			Session.getTransaction().commit();
			System.out.println("Registro insertado exitosamente");
			Session.close();
			return true;
		} catch (Exception e) {
			// TODO: handle exception

			return false;
		}

	}

	@SuppressWarnings("rawtypes")
	public Agencia buscarAgencia(int identificador) {

		Agencia agencia = null;

		System.out.println("Buscando agencia ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Agencia.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			q = Session.createQuery("from Agencia as agencia where agencia.idAgencia=" + identificador);

			agencia = (Agencia) ((org.hibernate.query.Query) q).uniqueResult();

			Session.getTransaction().commit();
			Session.close();
			return agencia;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}

	}

}

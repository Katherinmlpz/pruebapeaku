package modelo;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@Entity
@Table(name = "tb_aspirante")
public class Aspirante {
	
	public Aspirante() {
		super();
	}
	
	public Aspirante(int idAspirante, String nombre, int edad, String genero, int idProfesion, int idAgencia) {
		super();
		this.idAspirante = idAspirante;
		this.nombre = nombre;
		this.edad = edad;
		this.genero = genero;
		this.idProfesion = idProfesion;
		this.idAgencia = idAgencia;
	}
	
	@Id
	@Column(name = "IdAspirante")
	private int idAspirante;
	@Column(name = "AS_Nombre")
	private String nombre;
	@Column(name = "AS_Edad")
	private int edad;
	@Column(name = "AS_Genero")
	private String genero;
	@Column(name = "IdProfesion")
	private int idProfesion;
	@Column(name = "IdAgencia")
	private int idAgencia;
	public int getIdAspirante() {
		return idAspirante;
	}

	public void setIdAspirante(int idAspirante) {
		this.idAspirante = idAspirante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getIdProfesion() {
		return idProfesion;
	}

	public void setIdProfesion(int idProfesion) {
		this.idProfesion = idProfesion;
	}

	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	@Override
	public String toString() {
		return "Aspirante [idAspirante=" + idAspirante + ", nombre=" + nombre + ", edad=" + edad + ", genero=" + genero
				+ ", idProfesion=" + idProfesion + ", idAgencia=" + idAgencia + "]";
	}
	
	//METODOS
	
	public boolean registrarAspirante(Aspirante aspirante) {
		System.out.println("Registrando");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Aspirante.class).buildSessionFactory();
		Session Session = Factory.openSession();

		try {
			
			Session.beginTransaction();
			Session.save(aspirante);
			Session.getTransaction().commit();
			System.out.println("Registro insertado exitosamente");
			Session.close();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			
			return false;
		} 
		
	}
	
	@SuppressWarnings("rawtypes")
	public Aspirante buscarAspirante(int identificador) {

		Aspirante aspirante = null;

		System.out.println("Buscando aspirante ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Aspirante.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			
			q = Session.createQuery("from Aspirante as aspirante where aspirante.idAspirante=" + identificador);

			aspirante = (Aspirante) ((org.hibernate.query.Query) q).uniqueResult();

			Session.getTransaction().commit();
			Session.close();
			return aspirante;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}

	}
	
}

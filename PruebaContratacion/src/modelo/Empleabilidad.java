package modelo;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@Entity
@Table(name = "tb_empleabilidad")
public class Empleabilidad {
	
	
	public Empleabilidad() {
		super();
	}
	
	public Empleabilidad(int idAspirante, int idOferta, Date eFecha) {
		super();
		this.idAspirante = idAspirante;
		this.idOferta = idOferta;
		this.eFecha = eFecha;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;
	@Column(name = "IdAspirante")
	private int idAspirante;
	@Column(name = "IdOferta")
	private int idOferta;
	@Column(name = "eFecha")
	private Date eFecha;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdAspirante() {
		return idAspirante;
	}

	public void setIdAspirante(int idAspirante) {
		this.idAspirante = idAspirante;
	}

	public int getIdOferta() {
		return idOferta;
	}

	public void setIdOferta(int idOferta) {
		this.idOferta = idOferta;
	}

	public Date geteFecha() {
		return eFecha;
	}

	public void seteFecha(Date eFecha) {
		this.eFecha = eFecha;
	}

	@Override
	public String toString() {
		return "Empleabilidad [id=" + id + ", idAspirante=" + idAspirante + ", idOferta=" + idOferta + ", eFecha="
				+ eFecha + "]";
	}
	
	public boolean aplicarOferta(Empleabilidad empleabilidad) {
		System.out.println("Registrando apliación a oferta");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Empleabilidad.class).buildSessionFactory();
		Session Session = Factory.openSession();

		try {
			
			Session.beginTransaction();
			Session.save(empleabilidad);
			Session.getTransaction().commit();
			System.out.println("Registro insertado exitosamente");
			Session.close();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			
			return false;
		} 
		
	}
	
	@SuppressWarnings("rawtypes")
	public List<Empleabilidad> listaEmpleos(int identificador) {

		List<Empleabilidad> empleos = null;

		System.out.println("Buscando empleos ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Empleabilidad.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			if(identificador != 0) {
				q = Session.createQuery("from Empleabilidad as empleos where empleos.idAgencia=" + identificador);
			}else {
				q = Session.createQuery("from Oferta as ofertas");
			}
			

			empleos = (List<Empleabilidad>) ((org.hibernate.query.Query) q).getResultList();
			if(empleos.size()>0) {
				for (Empleabilidad empleo : empleos) {
					System.out.println(empleo.toString());
				}
				Session.getTransaction().commit();
				Session.close();
				return empleos;
			}else {
				Session.getTransaction().commit();
				Session.close();
				return null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Empleabilidad listaEmpleosU(int identificador) {

		System.out.println("Buscando empleos ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Empleabilidad.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();
        Empleabilidad empleos = new Empleabilidad();
		try {

			Query q = null;
			Session.beginTransaction();
			if(identificador != 0) {
				q = Session.createQuery("from Empleabilidad as empleos where empleos.idOferta=" + identificador);
			}else {
				q = Session.createQuery("from Empleabilidad as ofertas");
			}
			

			empleos = (Empleabilidad) ((org.hibernate.query.Query) q).getSingleResult();
			if(empleos != null) {
				Session.getTransaction().commit();
				Session.close();
				return empleos;
			}else {
				Session.getTransaction().commit();
				Session.close();
				return null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}
	}
}

package modelo;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@Entity
@Table(name = "tb_oferta")
public class Oferta {
	
	public Oferta() {
		super();
	}
	
	public Oferta(String nombre, String descripcion, Date fechaInicio, Date fechaFin) {
		super();
		this.Nombre = nombre;
		this.Descripcion = descripcion;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IdOferta")
	private int idOferta;
	@Column(name = "O_Nombre")
	private String Nombre;
	@Column(name = "O_Descripcion")
	private String Descripcion;
	@Column(name = "O_Fecha_inicio")
	private Date   fechaInicio;
	@Column(name = "O_Fecha_fin")
	private Date   fechaFin;
	@Column(name = "IdAgencia")
	private int idAgencia;
	@Column(name = "IdProfesion")
	private int idProfesion;
	
	public int getIdOferta() {
		return idOferta;
	}
	public void setIdOferta(int idOferta) {
		this.idOferta = idOferta;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	public int getIdProfesion() {
		return idProfesion;
	}

	public void setIdProfesion(int idProfesion) {
		this.idProfesion = idProfesion;
	}

	@Override
	public String toString() {
		return "Oferta [idOferta=" + idOferta + ", Nombre=" + Nombre + ", Descripcion=" + Descripcion + ", fechaInicio="
				+ fechaInicio + ", fechaFin=" + fechaFin + ", idAgencia=" + idAgencia + ", idProfesion=" + idProfesion
				+ "]";
	}
	
	public boolean registrarOferta(Oferta oferta) {
		System.out.println("Registrando Oferta");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Oferta.class).buildSessionFactory();
		Session Session = Factory.openSession();
		System.out.println(oferta.toString());
		try {
			
			Session.beginTransaction();
			Session.save(oferta);
			Session.getTransaction().commit();
			System.out.println("Registro insertado exitosamente");
			Session.close();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			
			return false;
		} 
		
	}
	
	@SuppressWarnings("rawtypes")
	public List<Oferta> listarOfertas(int identificador) {

		List<Oferta> ofertas = null;

		System.out.println("Buscando ofertas ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Oferta.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			if(identificador != 0) {
				q = Session.createQuery("from Oferta as ofertas where ofertas.idOferta=" + identificador);
			}else {
				q = Session.createQuery("from Oferta as ofertas");
			}
			

			ofertas = (List<Oferta>) ((org.hibernate.query.Query) q).getResultList();
			if(ofertas.size()>0) {
				for (Oferta oferta : ofertas) {
					System.out.println(oferta.toString());
				}
				Session.getTransaction().commit();
				Session.close();
				return ofertas;
			}else {
				Session.getTransaction().commit();
				Session.close();
				return null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public List<Oferta> listarOfertasProfesion(int identificador, int agencia) {

		List<Oferta> ofertas = null;

		System.out.println("Buscando ofertas ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Oferta.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			if(agencia != 0) {
				q = Session.createQuery("from Oferta as ofertas where ofertas.idProfesion=" + identificador + "and ofertas.idAgencia=" + agencia);
			}
			
			if(identificador!=0 && agencia==0){
				q = Session.createQuery("from Oferta as ofertas where ofertas.idProfesion=" + identificador);
			}
			

			ofertas = (List<Oferta>) ((org.hibernate.query.Query) q).getResultList();
			if(ofertas != null) {
				for (Oferta oferta : ofertas) {
					System.out.println("Ofertas" +oferta.toString());
				}
				Session.getTransaction().commit();
				Session.close();
				return ofertas;
			}else {
				System.out.println("No se encontraron Ofertas");
				Session.getTransaction().commit();
				Session.close();
				return null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public List<Oferta> listarOfertasA(int identificador) {

		List<Oferta> ofertas = null;

		System.out.println("Buscando ofertas ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Oferta.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			if(identificador != 0) {
				q = Session.createQuery("from Oferta as ofertas where ofertas.idAgencia=" + identificador);
			}else {
				q = Session.createQuery("from Oferta as ofertas");
			}
			

			ofertas = (List<Oferta>) ((org.hibernate.query.Query) q).getResultList();
			if(ofertas != null) {
				Session.getTransaction().commit();
				Session.close();
				return ofertas;
			}else {
				Session.getTransaction().commit();
				Session.close();
				return null;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}
	}

}

package modelo;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@Entity
@Table(name = "tb_usuarios")
public class Usuario {

	
	public Usuario() {
		super();
	}
	
	public Usuario(String contrasena, int idAspirante, int idAgencia) {
		super();
		this.contrasena = contrasena;
		this.idAspirante = idAspirante;
		this.idAgencia = idAgencia;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;
	@Column(name = "Contrasena")
	private String contrasena;
	@Column(name = "IdAspirante")
	private int idAspirante;
	@Column(name = "IdAgencia")
	private int idAgencia;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getcontrasena() {
		return contrasena;
	}

	public void setcontrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public int getIdAspirante() {
		return idAspirante;
	}

	public void setIdAspirante(int idAspirante) {
		this.idAspirante = idAspirante;
	}

	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", contrasena=" + contrasena + ", idAspirante=" + idAspirante + ", idAgencia="
				+ idAgencia + "]";
	}
	
	public boolean registrarUsuario(Usuario usuario) {
		System.out.println("Registrando Usuario");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		Session Session = Factory.openSession();

		try {
			
			Session.beginTransaction();
			Session.save(usuario);
			Session.getTransaction().commit();
			System.out.println("Registro insertado exitosamente");
			Session.close();
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			
			return false;
		} 
		
	}
	
	@SuppressWarnings("rawtypes")
	public Usuario iniciarSesion(Usuario usuario, int rol) {
		
		Usuario UsuarioEncontrado = null;
		
		System.out.println("Iniciando sesi�n ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Usuario.class).buildSessionFactory();
		Session Session = Factory.openSession();

		try {
			
			Query q = null;
			Session.beginTransaction();
			if(rol==1) {
			 q = Session.createQuery("from Usuario as usuario where usuario.idAspirante=" + usuario.getIdAspirante());
			}else if(rol == 2){
				 q = Session.createQuery("from Usuario as usuario where usuario.idAgencia=" + usuario.getIdAgencia());
			}
			
			UsuarioEncontrado = (Usuario) ((org.hibernate.query.Query) q).uniqueResult();
			System.out.println(UsuarioEncontrado.toString() + usuario.toString());
			if(UsuarioEncontrado.getcontrasena().equals(usuario.getcontrasena())) {
				System.out.println("Correcto: " + UsuarioEncontrado.toString());
	            Session.getTransaction().commit();
				Session.close();
				return UsuarioEncontrado;
			}else {
				return null;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Incorrecto: " + e.toString());
			return null;
		} 
		
	}
	
	
}

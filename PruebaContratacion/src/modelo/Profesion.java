package modelo;

import java.util.List;

import javax.persistence.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


@Entity
@Table(name = "tb_profesion")
public class Profesion {

	
	
	public Profesion() {
	}
	
	public Profesion(String pNombre) {
		this.pNombre = pNombre;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IdProfesion")
	private int idProfesion;
	@Column(name = "P_Nombre")
	private String pNombre;
	public int getIdProfesion() {
		return idProfesion;
	}


	public void setIdProfesion(int idProfesion) {
		this.idProfesion = idProfesion;
	}


	public String getpNombre() {
		return pNombre;
	}


	public void setpNombre(String pNombre) {
		this.pNombre = pNombre;
	}

	@Override
	public String toString() {
		return "Profesion [idProfesion=" + idProfesion + ", pNombre=" + pNombre + "]";
	}
	
	@SuppressWarnings("rawtypes")
	public List<Profesion> listarProfesiones(int identificador) {

		List<Profesion> profesiones = null;

		System.out.println("Buscando profesiones ...");
		SessionFactory Factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Profesion.class)
				.buildSessionFactory();
		Session Session = Factory.openSession();

		try {

			Query q = null;
			Session.beginTransaction();
			if(identificador != 0) {
				q = Session.createQuery("from Profesion as profesiones where profesiones.idProfesion=" + identificador);
			}else {
				q = Session.createQuery("from Profesion as profesiones");
			}
			

			profesiones = (List<Profesion>) ((org.hibernate.query.Query) q).getResultList();
			
			for (Profesion profesion : profesiones) {
				System.out.println(profesion.toString());
			}
			Session.getTransaction().commit();
			Session.close();
			return profesiones;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error: " + e.toString());
			return null;
		}

	}
	
}

package controlador;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.*;

public class Controlador extends HttpServlet {

	boolean error = false;
	String mess = "";

	private void doAction(HttpServletRequest request, boolean guardar, int opcion) {
		error = false;
		HttpSession session = request.getSession(true);
		if (opcion == 1) {
			Aspirante aspirante = new Aspirante();

			if (guardar) {
				aspirante.setIdAspirante(Integer.parseInt(request.getParameter("cedula")));
				aspirante.setNombre(request.getParameter("nombre"));
				aspirante.setEdad(Integer.parseInt(request.getParameter("edad")));
				aspirante.setGenero(request.getParameter("genero"));
				aspirante.setIdProfesion(Integer.parseInt(request.getParameter("profesion")));
			}

			if (guardar) {
				if (aspirante.registrarAspirante(aspirante)) {
					Usuario usuario = new Usuario(request.getParameter("contrasena"), aspirante.getIdAspirante(), 0);
					usuario.registrarUsuario(usuario);
				} else {
					error = true;
				}
			} else {
				Empleabilidad empleo = new Empleabilidad();
				List<Empleabilidad> empleos = new ArrayList<Empleabilidad>();
				List<Aspirante> aspirantes = new ArrayList<Aspirante>();
				Oferta oferta = new Oferta();
				List<Oferta> ofertas = oferta.listarOfertasA(Integer.valueOf(request.getParameter("idAgencia")));

				for (Oferta oferti : ofertas) {

					empleo = empleo.listaEmpleosU(oferti.getIdOferta());
					empleos.add(empleo);
					
					aspirante = aspirante.buscarAspirante(empleo.getIdAspirante());
					aspirantes.add(aspirante);
				}

				session.setAttribute("Aspirantes", aspirantes);
				session.setAttribute("OfertasAplicadas", empleos);
				session.setAttribute("Ofertas", ofertas);
			}
		} else if (opcion == 2) {
			Agencia agencia = new Agencia();

			agencia.setIdAgencia(Integer.parseInt(request.getParameter("nit")));
			agencia.setNombre(request.getParameter("nombre"));
			agencia.setTelefono(request.getParameter("telefono"));
			agencia.setDireccion(request.getParameter("direccion"));

			if (guardar) {
				if (agencia.registrarAgencia(agencia)) {
					Usuario usuario = new Usuario(request.getParameter("contrasena"), 0, agencia.getIdAgencia());
					usuario.registrarUsuario(usuario);
				} else {
					error = true;
				}
			}
		}

	}

	private void doLogin(HttpServletRequest request) {

		Usuario usuario = new Usuario();
		HttpSession session = request.getSession(true);
		int rol = Integer.parseInt(request.getParameter("rol"));
		if (rol == 1) {
			usuario.setIdAspirante(Integer.parseInt(request.getParameter("identificacion")));
		} else {
			usuario.setIdAgencia(Integer.parseInt(request.getParameter("identificacion")));
		}

		usuario.setcontrasena(request.getParameter("contrasena"));
		Usuario encontrado = usuario.iniciarSesion(usuario, Integer.parseInt(request.getParameter("rol")));

		if (encontrado != null) {

			Aspirante aspirante = new Aspirante();
			Agencia agencia = new Agencia();
			Profesion profesion = new Profesion();
			Oferta oferta = new Oferta();
			List<Profesion> profesiones = null;
			List<Oferta> ofertas = null;

			System.out.println("Inici� Sesi�n");
			if (rol == 1) {
				aspirante = aspirante.buscarAspirante(encontrado.getIdAspirante());
				profesiones = profesion.listarProfesiones(aspirante.getIdProfesion());

				ofertas = oferta.listarOfertasProfesion(aspirante.getIdProfesion(), 0);
			} else {
				agencia = agencia.buscarAgencia(encontrado.getIdAgencia());
				profesiones = profesion.listarProfesiones(0);
			}

			session.setAttribute("profesiones", profesiones);
			session.setAttribute("listaOfertasProfesion", ofertas);
			session.setAttribute("datos", (rol == 1) ? aspirante : agencia);
			session.setAttribute("rol", rol);
		} else {
			System.out.println("No inici� Sesi�n");
			error = true;
		}
	}

	private void listarOfertasProfesion(HttpServletRequest request) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		System.out.println("Buscando...");
		int idProfesion = Integer.valueOf(request.getParameter("Opcion"));
		int idAgencia = Integer.valueOf(request.getParameter("idAgencia"));
		session.setAttribute("idProfesion", idProfesion);
		session.setAttribute("idAgencia", idAgencia);
		Oferta oferta = new Oferta();

		List<Oferta> ofertas = (List<Oferta>) oferta.listarOfertasProfesion(idProfesion, idAgencia);

		if (ofertas != null) {
			session.setAttribute("listaOfertasProfesion", ofertas);
		}
	}

	private void doPublish(HttpServletRequest request) {

		HttpSession session = request.getSession(true);
		Oferta oferta = new Oferta();

		oferta.setNombre(request.getParameter("nombre"));
		oferta.setDescripcion(request.getParameter("descripcion"));
		oferta.setFechaInicio(Date.valueOf(request.getParameter("fecha_inicio")));
		oferta.setFechaFin(Date.valueOf(request.getParameter("fecha_fin")));
		oferta.setIdAgencia(Integer.valueOf(request.getParameter("idAgencia")));
		oferta.setIdProfesion(Integer.valueOf(request.getParameter("idProfesion")));

		if (oferta.registrarOferta(oferta)) {
			System.out.println("Almacenado con �xito");
			List<Oferta> ofertas = (List<Oferta>) oferta.listarOfertasProfesion(oferta.getIdProfesion(),
					oferta.getIdAgencia());
			if (ofertas != null) {
				session.setAttribute("listaOfertasProfesion", ofertas);
			}
		} else {
			error = true;
		}

	}

	private void doAplicar(HttpServletRequest request) {

		HttpSession session = request.getSession(true);
		Empleabilidad empleo = new Empleabilidad();

		empleo.setIdOferta(Integer.valueOf(request.getParameter("idOferta")));
		empleo.setIdAspirante(Integer.valueOf(request.getParameter("idAspirante")));
		empleo.seteFecha(Date.valueOf(request.getParameter("fecha")));

		Oferta oferta = new Oferta();

		if (empleo.aplicarOferta(empleo)) {
			System.out.println("Almacenado con �xito");
		} else {
			error = true;
		}

	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String address = "index.jsp";
		request.setCharacterEncoding("UTF-8");

		switch (request.getParameter("Opcion")) {
		case "RegistrarAspirante":
			address = "index.jsp";
			doAction(request, true, 1);
			if (error) {
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		case "RegistrarAgencia":
			address = "index.jsp";
			doAction(request, true, 2);
			if (error) {
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		case "IniciarSesion":
			address = "postLogin.jsp";
			doLogin(request);
			if (error) {
				address = "index.jsp";
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		case "Publicar":
			address = "ofertasP.jsp";
			doPublish(request);
			if (error) {
				address = "ofertasP.jsp";
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		case "Aplicar":
			address = "index.jsp";
			doAplicar(request);
			if (error) {
				address = "index.jsp";
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		case "Gestionar":
			address = "aspirantes.jsp";
			doAction(request, false, 1);
			if (error) {
				address = "aspirantes.jsp";
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		default:
			address = "ofertasP.jsp";
			listarOfertasProfesion(request);
			if (error) {
				address = "postLogin.jsp";
				HttpSession session = request.getSession();
				session.setAttribute("mess", mess);
			}
			break;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(address);

		dispatcher.forward(request, response);
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}

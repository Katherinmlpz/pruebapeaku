-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for agencia_empleo
DROP DATABASE IF EXISTS `agencia_empleo`;
CREATE DATABASE IF NOT EXISTS `agencia_empleo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `agencia_empleo`;

-- Dumping structure for table agencia_empleo.tb_agencia
DROP TABLE IF EXISTS `tb_agencia`;
CREATE TABLE IF NOT EXISTS `tb_agencia` (
  `IdAgencia` int(11) unsigned NOT NULL,
  `Ag_Nombre` varchar(50) NOT NULL,
  `Ag_Telefono` varchar(16) NOT NULL,
  `Ag_Direccion` varchar(50) NOT NULL,
  PRIMARY KEY (`IdAgencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Es la entidad más importante del problema.';

-- Dumping data for table agencia_empleo.tb_agencia: ~3 rows (approximately)
/*!40000 ALTER TABLE `tb_agencia` DISABLE KEYS */;
INSERT INTO `tb_agencia` (`IdAgencia`, `Ag_Nombre`, `Ag_Telefono`, `Ag_Direccion`) VALUES
	(0, 'SIN AGENCIA', '0', '0'),
	(1111111, 'BIMBO', '3209215529', 'MZ 6 D D'),
	(52370919, 'COCACOLA', '320924578', 'AQUÍ');
/*!40000 ALTER TABLE `tb_agencia` ENABLE KEYS */;

-- Dumping structure for table agencia_empleo.tb_aspirante
DROP TABLE IF EXISTS `tb_aspirante`;
CREATE TABLE IF NOT EXISTS `tb_aspirante` (
  `IdAspirante` int(11) unsigned NOT NULL,
  `AS_Nombre` varchar(50) NOT NULL,
  `AS_Edad` int(2) unsigned NOT NULL,
  `AS_Genero` char(1) NOT NULL,
  `IdProfesion` int(11) unsigned DEFAULT NULL,
  `IdAgencia` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`IdAspirante`),
  KEY `FK_PROFESION_ASPIRANTE` (`IdProfesion`),
  KEY `FK_AGENCIA_ASPIRANTE` (`IdAgencia`),
  CONSTRAINT `FK_AGENCIA_ASPIRANTE` FOREIGN KEY (`IdAgencia`) REFERENCES `tb_agencia` (`IdAgencia`),
  CONSTRAINT `FK_tb_aspirante_tb_profesion` FOREIGN KEY (`IdProfesion`) REFERENCES `tb_profesion` (`IdProfesion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Es la entidad que contiene la información de cada uno de los aspirantes.';

-- Dumping data for table agencia_empleo.tb_aspirante: ~4 rows (approximately)
/*!40000 ALTER TABLE `tb_aspirante` DISABLE KEYS */;
INSERT INTO `tb_aspirante` (`IdAspirante`, `AS_Nombre`, `AS_Edad`, `AS_Genero`, `IdProfesion`, `IdAgencia`) VALUES
	(0, 'NN', 0, 'N', 1, NULL),
	(454545, 'El abogado', 56, 'M', 1, 0),
	(1088033800, 'Katherin Muñoz Lopez', 23, 'F', 2, 0),
	(1225092761, 'Sebastian Lopez H', 22, 'M', 1, 0);
/*!40000 ALTER TABLE `tb_aspirante` ENABLE KEYS */;

-- Dumping structure for table agencia_empleo.tb_empleabilidad
DROP TABLE IF EXISTS `tb_empleabilidad`;
CREATE TABLE IF NOT EXISTS `tb_empleabilidad` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IdAspirante` int(11) unsigned NOT NULL,
  `IdOferta` int(11) unsigned NOT NULL,
  `eFecha` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ASPIRANTE_EMPLEABILIDAD` (`IdAspirante`),
  KEY `FK_tb_empleabilidad_tb_oferta` (`IdOferta`),
  CONSTRAINT `FK_tb_empleabilidad_tb_aspirante` FOREIGN KEY (`IdAspirante`) REFERENCES `tb_aspirante` (`IdAspirante`),
  CONSTRAINT `FK_tb_empleabilidad_tb_oferta` FOREIGN KEY (`IdOferta`) REFERENCES `tb_oferta` (`IdOferta`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='Esta entidad registra la información del aspirante y la oferta de la agencia.';

-- Dumping data for table agencia_empleo.tb_empleabilidad: ~2 rows (approximately)
/*!40000 ALTER TABLE `tb_empleabilidad` DISABLE KEYS */;
INSERT INTO `tb_empleabilidad` (`Id`, `IdAspirante`, `IdOferta`, `eFecha`) VALUES
	(9, 1088033800, 25, '2021-08-08');
/*!40000 ALTER TABLE `tb_empleabilidad` ENABLE KEYS */;

-- Dumping structure for table agencia_empleo.tb_oferta
DROP TABLE IF EXISTS `tb_oferta`;
CREATE TABLE IF NOT EXISTS `tb_oferta` (
  `IdOferta` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `O_Nombre` varchar(50) NOT NULL,
  `O_Descripcion` tinytext NOT NULL,
  `O_Fecha_inicio` date NOT NULL,
  `O_Fecha_fin` date NOT NULL,
  `IdAgencia` int(11) unsigned DEFAULT NULL,
  `IdProfesion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`IdOferta`),
  KEY `FK_AGENCIA` (`IdAgencia`),
  KEY `FK_PROFESION` (`IdProfesion`),
  CONSTRAINT `FK_AGENCIA` FOREIGN KEY (`IdAgencia`) REFERENCES `tb_agencia` (`IdAgencia`),
  CONSTRAINT `FK_PROFESION` FOREIGN KEY (`IdProfesion`) REFERENCES `tb_profesion` (`IdProfesion`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='Registro que muestra las características de la oferta\r.';

-- Dumping data for table agencia_empleo.tb_oferta: ~3 rows (approximately)
/*!40000 ALTER TABLE `tb_oferta` DISABLE KEYS */;
INSERT INTO `tb_oferta` (`IdOferta`, `O_Nombre`, `O_Descripcion`, `O_Fecha_inicio`, `O_Fecha_fin`, `IdAgencia`, `IdProfesion`) VALUES
	(25, 'SE NECESITA INGENIERO', 'Ingeniero con 5 años de experiencia', '2021-08-08', '2021-08-10', 52370919, 2),
	(26, 'se necesita ADM EMPRESAS', 'Empresas con 5 años de experiencia', '2021-08-14', '2021-08-14', 1111111, 4);
/*!40000 ALTER TABLE `tb_oferta` ENABLE KEYS */;

-- Dumping structure for table agencia_empleo.tb_profesion
DROP TABLE IF EXISTS `tb_profesion`;
CREATE TABLE IF NOT EXISTS `tb_profesion` (
  `IdProfesion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `P_Nombre` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`IdProfesion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Es la entidad que contiene la información de las profesiones de la agencia.';

-- Dumping data for table agencia_empleo.tb_profesion: ~4 rows (approximately)
/*!40000 ALTER TABLE `tb_profesion` DISABLE KEYS */;
INSERT INTO `tb_profesion` (`IdProfesion`, `P_Nombre`) VALUES
	(1, 'Abogado'),
	(2, 'Ingeniería de Sistemas'),
	(3, 'Administrador de Empresas'),
	(4, 'Psicologo');
/*!40000 ALTER TABLE `tb_profesion` ENABLE KEYS */;

-- Dumping structure for table agencia_empleo.tb_usuarios
DROP TABLE IF EXISTS `tb_usuarios`;
CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Contrasena` varchar(8) NOT NULL DEFAULT '',
  `IdAspirante` int(11) unsigned DEFAULT NULL,
  `IdAgencia` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ASPIRANTE` (`IdAspirante`),
  KEY `FK_AGENCIA` (`IdAgencia`),
  CONSTRAINT `FK_ASPIRANTE` FOREIGN KEY (`IdAspirante`) REFERENCES `tb_aspirante` (`IdAspirante`),
  CONSTRAINT `FK_tb_usuarios_tb_agencia` FOREIGN KEY (`IdAgencia`) REFERENCES `tb_agencia` (`IdAgencia`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Entidad que guarda los datos de login';

-- Dumping data for table agencia_empleo.tb_usuarios: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_usuarios` DISABLE KEYS */;
INSERT INTO `tb_usuarios` (`Id`, `Contrasena`, `IdAspirante`, `IdAgencia`) VALUES
	(1, '123456', 1088033800, 0),
	(2, '123456', 1225092761, 0),
	(4, '123456', 0, 52370919),
	(5, '123456', 454545, 0),
	(6, '123456', 0, 1111111);
/*!40000 ALTER TABLE `tb_usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
